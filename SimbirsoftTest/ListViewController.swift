//
//  ListViewController.swift
//  SimbirsoftTest
//
//  Created by  Max Rv on 23.04.2021.
//

import UIKit
import CoreData


// можно вынести класс ячейки в отдельный файл, но не обязательно:

class CustomCellVC: UITableViewCell {
    
    @IBOutlet weak var dateInCell: UILabel!
    @IBOutlet weak var taskInCell: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
}









class ListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate {
    
    
    @IBOutlet private var tableView: UITableView!
    
    var fetchResultsController: NSFetchedResultsController<Task>!

    var tasks: [Task] = []
    
//        Task(taskName: "Купить слона", taskDate: "25 апреля 2021", taskTime: "12:10", taskDetails: "Купить индийского или африканского слона не моложе 3 лет и не старше 5 лет. Лучше мальчика, чем девочку."),
//        Task(taskName: "Спасти мир", taskDate: "26 апреля 2021", taskTime: "05:00", taskDetails: "Желательно при этом не погибнуть самому. Допускается в качестве группы поддержки использовать Брюса Виллиса и Терминатора."),
//        Task(taskName: "Выучить Swift", taskDate: "27 апреля 2021", taskTime: "21:00", taskDetails: "Знать MVC, MVVM, Combine, SwiftUI, constraints и много других страшных слов."),
//        Task(taskName: "Написать приложение iTask", taskDate: "27 апреля 2021", taskTime: "23:59", taskDetails: "загрузить на git (github или bitbucket), если с гитом не в ладах, то отправить исходники в архиве по e-mail.")
//    ]
    
    
    
    // Что в ячейке:
    func tableView(_ myTableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = myTableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CustomCellVC
        cell.dateInCell.text = tasks[indexPath.row].time
        cell.taskInCell.text = tasks[indexPath.row].name
        return cell
    }
    
    // сколько ячеек:
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tasks.count
    }
    
    // Чтобы выделение строк не оставалось:
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Запрос к CoreData (получение данных):
        let fetchRequest: NSFetchRequest<Task> = Task.fetchRequest()
        
        let sortDescriptor = NSSortDescriptor(key: "name", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        if let context = (UIApplication.shared.delegate as? AppDelegate)?.coreDataStack.persistentContainer.viewContext {
            fetchResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
            fetchResultsController.delegate = self
            
            do {
                try fetchResultsController.performFetch()
                tasks = fetchResultsController.fetchedObjects!
            }
            catch let error as NSError {
                print(error.localizedDescription)
            }
            
        }
    }
    
    
    
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch type {
        case .insert: guard let indexPath = newIndexPath else {break}
            tableView.insertRows(at: [indexPath], with: .fade)
            
        case .delete: guard let indexPath = newIndexPath else {break}
            tableView.deleteRows(at: [indexPath], with: .fade)
            
        case .update: guard let indexPath = newIndexPath else {break}
            tableView.reloadRows(at: [indexPath], with: .fade)
            
        case .move:
            break
            
        @unknown default:
            tableView.reloadData()
        }
        tasks = controller.fetchedObjects as! [Task]
    }
    
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
    
    
    // Удаление строки:
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            self.tasks.remove(at: indexPath.row)
        }
        
       // tableView.reloadData()
        tableView.deleteRows(at: [indexPath], with: .fade)
        
        // Удаление из Core Data:
        if let context = (UIApplication.shared.delegate as? AppDelegate)?.coreDataStack.persistentContainer.viewContext {
            let taskToDelete = fetchResultsController.object(at: indexPath)
            context.delete(taskToDelete)
            
            do {
                try context.save()
            } catch {
                print(error.localizedDescription)
            }
        }
        
        
    }
    
    
    
    
    
    
    
    
   
    // Подготовка для передачи данных между двумя VC:
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "TaskSegue" {
            if let indexPath = tableView.indexPathForSelectedRow,
               let dvc = segue.destination as? TaskDetailsViewController {
                // структура формируется в соответствии с индексом выбранной ячейки таблицы:
                dvc.task = tasks[indexPath.row]
            }
        }
    }
    

    
}

