//
//  AddTaskViewController.swift
//  SimbirsoftTest
//
//  Created by  Max Rv on 23.04.2021.
//

import UIKit

class AddTaskViewController: UIViewController {
    
    @IBOutlet weak var addTaskName: UITextField!
    
    @IBOutlet weak var addTaskDateTime: UIDatePicker!
    
    @IBOutlet weak var addTaskText: UITextView!
    
    
    @IBAction func addTaskBtn(_ sender: UIButton) {
        if addTaskName.text == "" || addTaskText.text == "" {
            // print("Не все данные внесены!")
            let alert1 = UIAlertController(title: "Не все данные внесены!", message: nil, preferredStyle: .alert)
            alert1.addAction(UIAlertAction(title: "Попробовать снова", style: .default, handler: { action in
            }))
            self.present(alert1, animated: true)
            
        } else {
            
            if let context = (UIApplication.shared.delegate as? AppDelegate)?.coreDataStack.persistentContainer.viewContext {
                let task = Task(context: context)
                task.name = addTaskName.text
                // task.date = String(addTaskDateTime.date)
                task.fulltext = addTaskText.text
                
                do {
                    try context.save()
                    // print("Данные сохранены!")
                    let alert1 = UIAlertController(title: "Дело сохранено!", message: nil, preferredStyle: .alert)
                    alert1.addAction(UIAlertAction(title: "ОК", style: .default, handler: { action in
                    }))
                    self.present(alert1, animated: true)
                    // Очищаем поля:
                    addTaskName.text = ""
                    addTaskText.text = ""
                }
                catch let error as NSError {
                    print("Не удалось сохранить \(error), \(error.userInfo)")
                }
            }
            
            
            
        }
        
    }
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Граница для текстового поля:
        addTaskText.layer.borderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        addTaskText.layer.borderWidth = 0.5
        addTaskText.layer.cornerRadius = 5
        
    }

    // скрытие клавиатуры:
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    self.view.endEditing(true)
    }
    

}

