//
//  TaskDetailsViewController.swift
//  SimbirsoftTest
//
//  Created by  Max Rv on 23.04.2021.
//

import UIKit
import CoreData

class TaskDetailsViewController: UIViewController, NSFetchedResultsControllerDelegate {

    var fetchResultsController: NSFetchedResultsController<Task>!
    
    @IBOutlet weak var taskName: UILabel!
    @IBOutlet weak var srok: UITextField!
    @IBOutlet weak var taskFullText: UILabel!
    
    var task: Task?
    
    override func viewDidAppear(_ animated: Bool) {
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        taskName.text = task?.name
        srok.text = "\(task!.time)     \(task!.date)"
        taskFullText.text = task?.fulltext
    }
    
    
    
    @IBAction func deleteTaskBtn(_ sender: UIButton) {
         
        let alert3 = UIAlertController(title: "Эта кнопка пока не умеет удалять!", message: nil, preferredStyle: .alert)
        alert3.addAction(UIAlertAction(title: "ОК  :(", style: .default, handler: { action in
        }))
        self.present(alert3, animated: true)
        
        
        
//        // этот код удаляет все-все таски:
//        let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Task")
//        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetch)
//        if let context = (UIApplication.shared.delegate as? AppDelegate)?.coreDataStack.persistentContainer.viewContext {
//            do {
//                try context.execute(deleteRequest)
//            } catch {
//                print(error.localizedDescription)
//            }
//        }
            
//
        
    }
    
    

}
